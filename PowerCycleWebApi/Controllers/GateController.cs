﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PowerCycleWebApi
{
    public class GateController : ApiController
    {
        DB db;

        private GateController()
        {
            db = new DB();
        }

        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        [Route("api/addMember")]
        public IHttpActionResult AddMember()
        {
            return Ok();
        }

        [Route("api/initDB")]
        public IHttpActionResult postinitDB()
        {
            if(db.initDB())
            {
                return Ok();
            } else
            {
                return BadRequest("Failed to Init DB");
            }
        }

        [Route("api/signup/{username}/{password}")]
        public IHttpActionResult signup(string username, string password)
        {
            String response = db.signup(username, password);

            if(response.Equals("Signup Successful"))
            {
                return Ok();
            } else
            {
                return BadRequest(response);
            }
        }

        [HttpGet]
        [Route("api/test")]
        public IEnumerable<String> testConnection()
        {
            return db.getAll();
            
        }
        [Route("api/response")]
        public String getResponse()
        {
            return "asd";
        }

        [Route("api/login/{username}/{password}")]
        public IDictionary<string, object> login(string username, string password)
        {
            return db.login(username, password);
            
        }

        [Route("api/users")]
        public IEnumerable<Dictionary<String, object>> getUsers()
        {
            return db.getUsers();
        }
    }
}
