﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SQLite;
using System.IO;

namespace PowerCycleWebApi
{
    public class DB
    {
        SQLiteConnection dbConnection = new SQLiteConnection("Data Source = D:/powercycle.sqlite;Version=3;");
        
        public DB()
        {
            try
            {
                dbConnection.Close();
                dbConnection.Open();
            } catch (Exception e)
            {
                Console.WriteLine("---------" + e.ToString());
                initDB();
            }
        }

        public bool initDB()
        {
            try
            {
                SQLiteConnection.CreateFile("D:/powercycle.sqlite");
            } catch (Exception e)
            {
                Console.WriteLine("----------" + e.ToString());
            }
            string command = File.ReadAllText(System.IO.Directory.GetCurrentDirectory() + "/schema.sql");
            dbConnection = new SQLiteConnection("Data Source=D:/powercycle.sqlite;Version=3;");

            using (dbConnection)
            {
                using (SQLiteCommand dbCommand = dbConnection.CreateCommand())
                {
                    dbConnection.Open();
                    dbCommand.CommandText = command;
                    dbCommand.ExecuteNonQuery();

                    return true;
                }
            }
        }

        public List<string> getAll()
        {
            string query = "SELECT * FROM membership";
            SQLiteCommand comm = new SQLiteCommand(query, dbConnection);

            SQLiteDataReader reader = comm.ExecuteReader();
            List<String> res = new List<string>();

            while(reader.Read())
            {
                res.Add("User ID: " + reader["userid"] + " Points: " + reader["points"] + " Km: " + reader["km"]);
            }

            return res;
        }

        public string signup(string username, string password)
        {
            String query = "SELECT userid FROM membership";
            SQLiteCommand comm = new SQLiteCommand(query, dbConnection);
            SQLiteDataReader reader = comm.ExecuteReader();

            List<String> idList = new List<string>();
            while(reader.Read())
            {
                idList.Add((String) reader["userid"]);
            }

            if(idList.Contains(username))
            {
                return "Username is already in use";
            }

            query = "INSERT INTO credentials (userid, password) values ('" + username + "', '" + password + "')";
            comm = new SQLiteCommand(query, dbConnection);
            comm.ExecuteNonQuery();

            query = "INSERT INTO membership (userid, points, km, time) values ('" + username + "', 0, 0, 0)";
            comm = new SQLiteCommand(query, dbConnection);
            comm.ExecuteNonQuery();

            return "Signup Successful";
        }

        public string addBike(String bikeID)
        {
            String query = "SELECT bikeid FROM bike";
            SQLiteCommand comm = new SQLiteCommand(query, dbConnection);
            SQLiteDataReader reader = comm.ExecuteReader();

            List<String> bikeList = new List<string>();
            while(reader.Read())
            {
                bikeList.Add((String)reader["bikeid"]);
            }

            if(bikeList.Contains(bikeID))
            {
                return "Bike ID is already in use";
            }

            query = "INSERT INTO bike (bikeid, SOC, km, rent_timestamp, return_timestamp) values('" + bikeID + "', 0, 0, '', ''";
            comm = new SQLiteCommand(query, dbConnection);
            comm.ExecuteNonQuery();

            return "New Bike Added";
        }

        public Dictionary<String, object> login(string username, string password)
        {
            Dictionary<String, object> res = new Dictionary<String, object>();
            string query = "SELECT password FROM credentials WHERE userid = '" + username + "'";
            SQLiteCommand comm = new SQLiteCommand(query, dbConnection);
            SQLiteDataReader reader = comm.ExecuteReader();

            reader.Read();
            if(!password.Equals(reader["password"]))
            {
                res.Add("response", "Incorrect Password");
                return res;
            } else
            {
                res.Add("response", "OK");
                query = "SELECT DISTINCT * FROM membership WHERE userid = " + username;
                comm = new SQLiteCommand(query, dbConnection);
                reader = comm.ExecuteReader();
                reader.Read();
                res = Enumerable.Range(0, reader.FieldCount).ToDictionary(reader.GetName, reader.GetValue);
                return res;
            }
        }

        public List<Dictionary<String, object>> getUsers()
        {
            String query = "SELECT * from membership";
            SQLiteCommand comm = new SQLiteCommand(query, dbConnection);
            SQLiteDataReader reader = comm.ExecuteReader();
            List<Dictionary<String, object>> userList = new List<Dictionary<String, object>>();
            while(reader.Read())
            {
                userList.Add(Enumerable.Range(0, reader.FieldCount).ToDictionary(reader.GetName, reader.GetValue));
            }

            return userList;
        }

        public string getServerResponse()
        {
            return "Responded";
        }

    }
}