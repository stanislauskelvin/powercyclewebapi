drop table if exists entries;
create table entries (
	id integer primary key autoincrement,
	title text not null,
	'text' text not null
);

drop table if exists credentials;
create table credentials (
	userid text not null primary key,
	password text not null
);

drop table if exists info;
create table info (
	userid text not null,
	firstName text not null,
	lastName text not null,
	streetNumber integer,
	streetName text,
	streetPost text,
	phoneNumber integer,
	email text,
	FOREIGN KEY (userid) REFERENCES credentials(userid)
);

drop table if exists membership;
create table membership (
	userid text not null,
	points integer,
	km real,
	time integer,
	FOREIGN KEY(userid) REFERENCES credentials(userid)
);